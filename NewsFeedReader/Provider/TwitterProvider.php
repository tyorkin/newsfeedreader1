<?php
namespace NewsFeedReader\Provider;

use NewsFeedReader\Exception\BadRequestException;

class TwitterProvider extends BaseNewsFeedReaderProvider
{
    const PROVIDER_NAME = 'tw';
    const API_VERSION = '1.1';
    const NEWS_FEED_URL = 'https://api.twitter.com/'.self::API_VERSION.'/statuses/user_timeline.json';
    const OAUTH_VERSION = '1.0';

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function requestNewsFeed(array $params = [])
    {
        $fullParams = array_merge($this->credentials, $params);

        $news = $this->client->request(self::NEWS_FEED_URL, 'get', $fullParams);
    }
}