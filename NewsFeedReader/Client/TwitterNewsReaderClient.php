<?php
namespace NewsFeedReader\Client;


use NewsFeedReader\Exception\BadRequestException;

class TwitterNewsReaderClient extends BaseClient
{

    /** @var array  */
    private $params = [];

    /** @var string  */
    private $requestUrl = '';


    /**
     * @return string
     */
    private function buildOAuthSignature()
    {
        $oauthHash = '';
        if (!empty($this->params['count'])) {
            $oauthHash .= 'count=' . $this->params['count'] . '&';
        }
        $oauthHash .= 'oauth_consumer_key=' . $this->params['consumer_key'] . '&';
        $oauthHash .= 'oauth_nonce=' . time() . '&';
        $oauthHash .= 'oauth_signature_method=HMAC-SHA1&';
        $oauthHash .= 'oauth_timestamp=' . time() . '&';
        $oauthHash .= 'oauth_token=' . $this->params['access_token'] . '&';
        $oauthHash .= 'oauth_version=1.0';
        if (!empty($this->params['screen_name'])) {
            $oauthHash .=  '&screen_name=' . $this->params['screen_name'];
        }
        $base = '';
        $base .= 'GET';
        $base .= '&';
        $base .= rawurlencode($this->requestUrl);
        $base .= '&';
        $base .= rawurlencode($oauthHash);
        $key = '';
        $key .= rawurlencode($this->params['consumer_secret']);
        $key .= '&';
        $key .= rawurlencode($this->params['access_token_secret']);

        $signature = base64_encode(hash_hmac('sha1', $base, $key, true));
        $rawSignature = rawurlencode($signature);

        return $rawSignature;
    }

    /**
     * @return string
     */
    private function buildOAuthHeader()
    {
        $signature = $this->buildOAuthSignature();
        $oauthHeader = '';
        if (!empty($this->params['count'])) {
            $oauthHeader .= 'count="' . $this->params['count'] . '", ';
        }
        $oauthHeader .= 'oauth_consumer_key="' . $this->params['consumer_key'] . '", ';
        $oauthHeader .= 'oauth_nonce="' . time() . '", ';
        $oauthHeader .= 'oauth_signature="' . $signature . '", ';
        $oauthHeader .= 'oauth_signature_method="HMAC-SHA1", ';
        $oauthHeader .= 'oauth_timestamp="' . time() . '", ';
        $oauthHeader .= 'oauth_token="' . $this->params['access_token'] . '", ';
        $oauthHeader .= 'oauth_version="1.0", ';
        if (!empty($this->params['screen_name'])) {
            $oauthHeader .= 'screen_name="' . $this->params['screen_name'] . '", ';
        }

        return $oauthHeader;
    }

    protected function get($url, $params)
    {
        $this->params = $params;
        $this->requestUrl = $url;
        $oAuthHeader = $this->buildOAuthHeader();
        $curlHeader = array("Authorization: Oauth {$oAuthHeader}", 'Expect:');
        $curlRequest = curl_init();
        curl_setopt($curlRequest, CURLOPT_HTTPHEADER, $curlHeader);
        curl_setopt($curlRequest, CURLOPT_HEADER, false);
        $requestUrl = $this->normalizeUrl($this->requestUrl, $this->params);
        curl_setopt($curlRequest, CURLOPT_URL, $requestUrl);
        curl_setopt($curlRequest, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlRequest, CURLOPT_SSL_VERIFYPEER, false);
        $json = curl_exec($curlRequest);
        $httpStatus = curl_getinfo($curlRequest, CURLINFO_HTTP_CODE);
        $curlError = curl_error($curlRequest);
        $curlErrorNum = curl_errno($curlRequest);
        curl_close($curlRequest);
        if ($json === false) {
            throw new BadRequestException($curlError, $curlErrorNum);
        }
        $data = json_decode($json);
        if ($httpStatus !== 200) {
            $message = 'Error during receive response';
            $code = 0;
            if (json_last_error() == JSON_ERROR_NONE && isset($data->errors) && is_array($data->errors)) {
                $message = $data->errors[0]->message;
                $code = $data->errors[0]->code;
            }
            throw new BadRequestException(
                $message,
                $code
            );
        }


        return $data;
    }

    protected function post($url, $params)
    {
        // TODO: Implement post() method.
        return null;
    }
}