<?php

namespace NewsFeedReader\Client;


abstract class BaseClient implements ClientInterface
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    /**
     *@param string $url
     *@param string $method
     *@param array $params
     *
     * @return mixed
     */
    public function request($url, $method, $params)
    {
        $result = null;
        switch (strtoupper($method)) {

            case self::METHOD_GET:
                $result = $this->get($url, $params);
                break;
            case self::METHOD_POST:
                $result =  $this->post($url, $params);
                break;
        }
        return $result;
    }

    /**
     * @param string $url
     * @param array $parameters
     *
     * @return string
     */
    protected function normalizeUrl($url, array $parameters = array())
    {
        $normalizedUrl = $url;
        if (!empty($parameters)) {
            $normalizedUrl .= (false !== strpos($url, '?') ? '&' : '?') . http_build_query($parameters, '', '&');
        }

        return $normalizedUrl;
    }

    abstract protected function get($url, $params);
    abstract protected function post($url, $params);
}